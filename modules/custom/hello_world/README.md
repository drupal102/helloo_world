Bonjour,

Nous vous proposons de réaliser deux exercices pour vous permettre d’exprimer vos compétences et votre expérience.

Vous serez autorisé à utiliser toutes les ressources nécessaires, comme vous le feriez dans votre quotidien. Nous voulons nous assurer que vous êtes dans des conditions proches de votre environnement de travail habituel, afin que vous puissiez démontrer vos compétences et votre capacité à résoudre les problèmes de manière efficace.

Lors de notre entretien oral, vous nous expliquerez votre raisonnement, les facteurs qui ont influencé vos décisions, les avantages et inconvénients de chaque option.

**Idéalement il faut réaliser votre travail dans le module hello_world.**
Si vous avez des fichiers de configuration vous pouvez les mettre dans le module pour qu’ils soient pris en compte à son activation.

**Nous allons tester votre rendu avec un environement Drupal 10.1.x/PHP 8.2.x**

### Exercice 1

Activer le module hello_world.

Il va créer un type de contenu Poi :
-	Un champ Street non obligatoire.
-	Un champ Zip non obligatoire.
-	Un champ City non obligatoire.
-	Un champ Foo obligatoire avec une valeur par defaut.
-	Un champ Bar obligatoire avec une valeur par defaut.

Deux display mode pour le type de contenu Poi :
-	Display 1
- Display 2

Une vue Poi qui cré trois urls qui permettent de visualiser la liste des POIs dans ces display mode:
- /poi -> default
- /poi/1 -> Display 1
- /poi/2 -> Display 2

#### TODO

Au rendu d'un node il faut afficher l'adresse.

**Dans chaque display mode les champs Street, Zip et City ont été desactivé. Il ne faut pas les réactiver.**

#### Règles de gestion

L'adresse se construit comme cela :

Street - Zip - City

Si le champ Street est vide on affiche :
Zip - City

Si le champ Zip est vide on affiche :
City

Si le champ City est vide on affiche :
"Adresse inconnue"

Pour chaque view mode le rendu du node sera different :
- Default
  - Address
  - Foo
  - Bar
- Display 1
  - Foo
  - Address
  - Bar
- Display 2
  - Foo
  - Bar
  - Address

### Exercice 2

Activer le module hello_world callback.

Il va créer un callback /api qui va retourner une reponse 'OK' apres un certain delais.
Ce callback a pour but de simuler une API partenaire qui pourrait avoir des lenteurs a répondre.

A la creation/modification d'un Poi il faut envoyer la valeur des champs Street, Zip et City au partenaire.

#### TODO

Sécuriser et optimiser le code qui s'occupe de l'envoi des données.

**Ne pas modifier le code du controller HelloWorldCallbackController**

Bon courage.
