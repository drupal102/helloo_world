<?php

namespace Drupal\hello_world_callback\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;

/**
 * Returns responses for Hello world callback routes.
 */
final class HelloWorldCallbackController {

  /**
   * Builds the response.
   */
  public function api(): CacheableJsonResponse {
    sleep(rand(0, 10));
    $response = new CacheableJsonResponse('OK');
    $metadata = $response->getCacheableMetadata();
    $metadata->setCacheMaxAge(0);
    $response->addCacheableDependency($metadata);
    return $response;
  }

}
